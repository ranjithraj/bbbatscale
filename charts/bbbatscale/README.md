# Kubernetes Helm Chart for OpenShift 4

This chart has openshift specific deploymentConfig and imageStream

**Important:** For every section, you need following env variables
```bash
# OpenShift server
OPENSHIFT_URL=

# OpenShift username
OPENSHIFT_USERNAME=

# OpenShift project name
OPENSHIFT_PROJECT_NAME=my-bbbatscale-testing

# Helm release name
HELM_RELEASE=staging

# Initial image repository
IMAGE=

# Initial image tag
IMAGE_TAG=
```

## OpenShift project setup
```bash
# OpenShift login and creating new project
oc login --server=$OPENSHIFT_URL -u $OPENSHIFT_USERNAME
oc new-project $OPENSHIFT_PROJECT_NAME
oc project $OPENSHIFT_PROJECT_NAME
```

## Install helm chart
**Important:** Before you install the helm chart, look into charts/bbbatscale/values.yaml and use your own values for the section *IMPORTANT VARIABLES*.
```bash
# Download dependencies
helm dependency update charts/bbbatscale

# Install helm chart
helm install -n $OPENSHIFT_PROJECT_NAME $HELM_RELEASE charts/bbbatscale

# Import image
oc import-image --from=$IMAGE:$IMAGE_TAG $HELM_RELEASE-bbbatscale:live --confirm
```

## Upgrade helm chart
```bash
# Get current postgresql password
POSTGRESQL_PASSWORD=$(oc get secret --namespace $OPENSHIFT_PROJECT_NAME $HELM_RELEASE-postgresql -o jsonpath="{.data.postgresql-password}" | base64 --decode)

# Upgrade helm chart
helm upgrade $HELM_RELEASE charts/bbbatscale -n $OPENSHIFT_PROJECT_NAME --set postgresql.postgresqlPassword=$POSTGRESQL_PASSWORD
```

## Upgrade image
```bash
# New image repo
IMAGE_NEW=

# New image tag
IMAGE_TAG_NEW=

# Import new image
oc import-image --from=$IMAGE_NEW:$IMAGE_TAG_NEW $HELM_RELEASE-bbbatscale:$IMAGE_TAG_NEW --confirm
# Activate new image
oc tag $HELM_RELEASE-bbbatscale:$IMAGE_TAG_NEW $HELM_RELEASE-bbbatscale:live
```