from typing import Dict

from django.http import HttpRequest

from support_chat.models import SupportChatParameter


def message_max_length(request: HttpRequest) -> Dict[str, int]:
    return {"support_chat_message_max_length": SupportChatParameter.load().message_max_length}
