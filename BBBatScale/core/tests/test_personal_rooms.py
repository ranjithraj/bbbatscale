from django.conf import settings
from django.contrib.auth.models import Group
from django.test import TestCase


from core.models import HomeRoom, Tenant, GeneralParameter, RoomConfiguration, User, PersonalRoom


class CreateHomeRoomTestCase(TestCase):
    def setUp(self):
        self.tenant_fbi = Tenant.objects.create(
            name="Fachbereich Informatik",
        )

        self.room_config_1 = RoomConfiguration.objects.create()

        self.general_parameters: GeneralParameter = GeneralParameter.load()
        self.general_parameters.home_room_enabled = True
        self.general_parameters.home_room_teachers_only = True
        self.general_parameters.home_room_tenant = self.tenant_fbi
        self.general_parameters.home_room_room_configuration = self.room_config_1
        self.general_parameters.save()

        self.moderator_group = Group.objects.create(name=settings.MODERATORS_GROUP)
        self.moderator_group.save()

    def tearDown(self):
        pass

    def test_home_room_create_with_email_name(self):
        self.teacher_user: User = User.objects.create_user(
            username="teacher-1",
            password="secret",
            email="teacher-1@nothing.com",
            first_name="Jane J.",
            last_name="Doe"
        )
        self.teacher_user.groups.add(self.moderator_group)
        self.teacher_user.save()

        homeroom = HomeRoom.objects.get(owner=self.teacher_user.pk)
        self.assertTrue("home-teacher-1@nothing.com" == homeroom.name)

    def test_home_room_create_with_email_not_set(self):
        self.teacher_user: User = User.objects.create_user(
            username="teacher-1",
            password="secret",
            first_name="Jane J.",
            last_name="Doe"
        )
        self.teacher_user.groups.add(self.moderator_group)
        self.teacher_user.save()

        homeroom = HomeRoom.objects.get(owner=self.teacher_user.pk)
        self.assertTrue("home-Doe-" in homeroom.name)

    def test_home_room_create_two_users_same_email(self):
        self.teacher_user_1: User = User.objects.create_user(
            username="teacher-1",
            password="secret",
            email="teacher-1@nothing.com",
            first_name="Jane J.",
            last_name="Doe"
        )
        self.teacher_user_1.groups.add(self.moderator_group)
        self.teacher_user_1.save()
        homeroom_1 = HomeRoom.objects.get(owner=self.teacher_user_1.pk)
        self.assertTrue("home-teacher-1@nothing.com" == homeroom_1.name)

        self.teacher_user_2: User = User.objects.create_user(
            username="teacher-2",
            password="secret",
            email="teacher-1@nothing.com",
            first_name="Jane J.",
            last_name="Doe"
        )
        self.teacher_user_2.groups.add(self.moderator_group)
        self.teacher_user_2.save()
        homeroom_2 = HomeRoom.objects.get(owner=self.teacher_user_2.pk)
        self.assertTrue("home-Doe-" in homeroom_2.name)

    def test_home_room_created_for_teacher_only(self):
        self.teacher_user: User = User.objects.create_user(
            username="teacher-1",
            password="secret",
            email="teacher-1@nothing.com",
            first_name="Jane J.",
            last_name="Doe"
        )
        self.teacher_user.groups.add(self.moderator_group)
        self.teacher_user.save()

        self.student_user: User = User.objects.create_user(
            username="student-1",
            password="secret",
            email="student-1@nothing.com",
            first_name="John S.",
            last_name="Doe"
        )

        self.assertTrue(HomeRoom.objects.filter(owner=self.teacher_user.pk).exists())
        self.assertFalse(HomeRoom.objects.filter(owner=self.student_user.pk).exists())

    def test_home_room_created_for_all_users(self):
        self.general_parameters.home_room_teachers_only = False
        self.general_parameters.save()

        self.teacher_user: User = User.objects.create_user(
            username="teacher-1",
            password="secret",
            email="teacher-1@nothing.com",
            first_name="Jane J.",
            last_name="Doe"
        )
        self.teacher_user.groups.add(self.moderator_group)
        self.teacher_user.save()

        self.student_user: User = User.objects.create_user(
            username="student-1",
            password="secret",
            email="student-1@nothing.com",
            first_name="John S.",
            last_name="Doe"
        )

        self.assertTrue(HomeRoom.objects.filter(owner=self.student_user.pk).exists())
        self.assertTrue(HomeRoom.objects.filter(owner=self.teacher_user.pk).exists())
        self.assertIsNotNone(self.teacher_user.homeroom)
        self.assertIsNotNone(self.student_user.homeroom)

    def test_home_room_delete_on_user_delete(self):
        self.teacher_user: User = User.objects.create_user(
            username="teacher-1",
            password="secret",
            email="teacher-1@nothing.com",
            first_name="Jane J.",
            last_name="Doe"
        )
        self.teacher_user.groups.add(self.moderator_group)
        self.teacher_user.save()

        self.student_user: User = User.objects.create_user(
            username="student-1",
            password="secret",
            email="student-1@nothing.com",
            first_name="John S.",
            last_name="Doe"
        )

        # check that users exist
        self.student_user = User.objects.filter(pk=self.student_user.pk)
        self.assertEqual(len(self.student_user), 1)

        self.teacher_user = User.objects.filter(pk=self.teacher_user.pk)
        self.assertEqual(len(self.teacher_user), 1)

        # check that their rooms exist / do not exist
        self.student_user_home = HomeRoom.objects.filter(owner=self.student_user[0].pk)
        self.assertEqual(len(self.student_user_home), 0)

        self.teacher_user_home = HomeRoom.objects.filter(owner=self.teacher_user[0].pk)
        self.assertEqual(len(self.teacher_user_home), 1)

        # delete users
        self.student_user.delete()
        self.teacher_user.delete()

        # check that no more rooms exist
        self.assertFalse(HomeRoom.objects.all().exists())

    def test_home_room_not_created(self):
        self.general_parameters.home_room_enabled = False
        self.general_parameters.save()

        self.teacher_user: User = User.objects.create_user(
            username="teacher-1",
            password="secret",
            email="teacher-1@nothing.com",
            first_name="Jane J.",
            last_name="Doe"
        )
        self.teacher_user.groups.add(self.moderator_group)
        self.teacher_user.save()

        self.student_user: User = User.objects.create_user(
            username="student-1",
            password="secret",
            email="student-1@nothing.com",
            first_name="John S.",
            last_name="Doe"
        )

        self.assertFalse(HomeRoom.objects.filter(owner=self.student_user.pk).exists())
        self.assertFalse(HomeRoom.objects.filter(owner=self.teacher_user.pk).exists())


class UpdateHomeRoomGeneralParametersTestCase(TestCase):
    def setUp(self):
        self.tenant_fbi = Tenant.objects.create(
            name="Fachbereich Informatik",
        )

        self.room_config_1 = RoomConfiguration.objects.create(
            name="config-1"
        )

        self.general_parameters: GeneralParameter = GeneralParameter.load()
        self.general_parameters.home_room_enabled = True
        self.general_parameters.home_room_teachers_only = True
        self.general_parameters.home_room_tenant = self.tenant_fbi
        self.general_parameters.home_room_room_configuration = self.room_config_1
        self.general_parameters.save()

        self.teacher_user: User = User.objects.create_user(
            username="teacher-1",
            password="secret",
            email="teacher-1@nothing.com",
            first_name="Jane J.",
            last_name="Doe"
        )

        self.moderator_group = Group.objects.create(name=settings.MODERATORS_GROUP)
        self.moderator_group.save()

        self.teacher_user.groups.add(self.moderator_group)
        self.teacher_user.save()

        self.student_user: User = User.objects.create_user(
            username="student-1",
            password="secret",
            email="student-1@nothing.com",
            first_name="John S.",
            last_name="Doe"
        )

    def tearDown(self):
        pass

    def test_home_room_enabled_false(self):
        self.general_parameters.home_room_enabled = False
        self.general_parameters.save()

        # All rooms were deleted
        self.assertFalse(HomeRoom.objects.all().exists())

    def test_home_room_enabled_true(self):
        self.general_parameters.home_room_enabled = False
        self.general_parameters.save()

        self.assertFalse(HomeRoom.objects.all().exists())

        self.general_parameters.home_room_enabled = True
        self.general_parameters.save()

        # Room was created only for teacher not for student
        self.assertEqual(len(HomeRoom.objects.all()), 1)

    def test_home_room_teachers_only_false(self):
        # Room existis only for teacher
        self.assertEqual(len(HomeRoom.objects.all()), 1)

        self.general_parameters.home_room_teachers_only = False
        self.general_parameters.save()

        # rooms exist for both student and teacher
        self.assertEqual(len(HomeRoom.objects.all()), 2)

    def test_home_room_teachers_only_true(self):
        # Room existis only for teacher
        self.assertEqual(len(HomeRoom.objects.all()), 1)

        self.general_parameters.home_room_teachers_only = False
        self.general_parameters.save()

        # rooms exist for both student and teacher
        self.assertEqual(len(HomeRoom.objects.all()), 2)

        self.general_parameters.home_room_teachers_only = True
        self.general_parameters.save()

        # rooms exist for teacher only
        self.assertEqual(len(HomeRoom.objects.all()), 1)

    def test_change_home_room_tenant(self):
        for home_room in HomeRoom.objects.all():
            self.assertEqual(home_room.tenant, self.tenant_fbi)

        tenant_2 = Tenant.objects.create(
            name="New Tenant",
        )

        self.general_parameters.home_room_tenant = tenant_2
        self.general_parameters.save()

        for home_room in HomeRoom.objects.all():
            self.assertEqual(home_room.tenant, tenant_2)

    def test_change_home_room_tenant_not_set(self):
        for home_room in HomeRoom.objects.all():
            self.assertEqual(home_room.tenant, self.tenant_fbi)

        tenant_2 = None

        self.general_parameters.home_room_tenant = tenant_2
        self.general_parameters.save()

        self.assertFalse(HomeRoom.objects.all().exists())

    def test_home_room_room_configuration(self):
        for home_room in HomeRoom.objects.all():
            self.assertEqual(home_room.config, self.room_config_1)

        room_config_2 = RoomConfiguration.objects.create(
            name="config-2"
        )

        self.general_parameters.home_room_room_configuration = room_config_2
        self.general_parameters.save()

        for home_room in HomeRoom.objects.all():
            self.assertEqual(home_room.config, room_config_2)

    def test_home_room_room_configuration_not_set(self):
        for home_room in HomeRoom.objects.all():
            self.assertEqual(home_room.config, self.room_config_1)

        room_config_2 = None

        self.general_parameters.home_room_room_configuration = room_config_2
        self.general_parameters.save()

        self.assertFalse(HomeRoom.objects.all().exists())


class CreatePersonalRoomsTestCase(TestCase):
    def setUp(self):
        self.tenant = Tenant.objects.create(
            name="FBI",
            token_registration="xxxxasdaxxcfaa"
        )
        gp: GeneralParameter = GeneralParameter.load()
        gp.personal_rooms_teacher_max_number = 8
        gp.personal_rooms_non_teacher_max_number = 0
        gp.home_room_tenant = self.tenant
        GeneralParameter.save(gp)
        self.moderator_group = Group.objects.create(name=settings.MODERATORS_GROUP)

        self.teacher_user_1: User = User.objects.create_user(
            username="teacher-1",
            password="secret",
            email="mailt1@nothing.com",
            first_name="Jane J.",
            last_name="Doe",
        )
        self.teacher_user_1.groups.add(self.moderator_group)
        self.teacher_user_1.save()

        self.teacher_user_2: User = User.objects.create_user(
            username="teacher-2",
            password="secret",
            email="mailt2@nothing.com",
            first_name="Jane J.",
            last_name="Doe",
            personal_rooms_max_number=3
        )
        self.teacher_user_2.groups.add(self.moderator_group)
        self.teacher_user_2.save()

        self.student_user_1: User = User.objects.create_user(
            username="student-1",
            password="secret",
            email="mails1@nothing.com",
            first_name="John S.",
            last_name="Doe",
        )

        self.student_user_2: User = User.objects.create_user(
            username="student-2",
            password="secret",
            email="mails2@nothing.com",
            first_name="John S.",
            last_name="Doe",
            personal_rooms_max_number=5
        )

        self.test_room_config = RoomConfiguration.objects.create()

    def tearDown(self):
        pass

    def test_personal_room_rooms_max_number(self):
        self.assertEqual(self.teacher_user_1.get_max_number_of_personal_rooms(), 8)
        self.assertEqual(self.teacher_user_2.get_max_number_of_personal_rooms(), 3)
        self.assertEqual(self.student_user_1.get_max_number_of_personal_rooms(), 0)
        self.assertEqual(self.student_user_2.get_max_number_of_personal_rooms(), 5)

    def test_owner_shall_not_be_co_owner(self):
        gp = GeneralParameter.load()

        pr_test = PersonalRoom(
            name="testroom",
            owner=self.teacher_user_1,
            tenant=gp.home_room_tenant,
            is_public=False,
            config=self.test_room_config
        )
        pr_test.save()

        pr_test.co_owners.add(pr_test.owner)
        pr_test.save()

        self.assertEqual(len(pr_test.co_owners.all()), 0)

    def test_personal_room_create_room(self):
        gp = GeneralParameter.load()

        pr_test = PersonalRoom(
            name="testroom",
            owner=self.teacher_user_1,
            tenant=gp.home_room_tenant,
            is_public=False,
            config=self.test_room_config,
        )
        pr_test.save()

        pr_test.co_owners.add(self.teacher_user_1)
        pr_test.co_owners.add(self.teacher_user_2)
        pr_test.co_owners.add(self.student_user_1)
        pr_test.co_owners.add(self.student_user_2)
        pr_test.save()

        print("Owner: ", pr_test.owner)
        print("Co_Owner: ", pr_test.co_owners.all())

        self.assertEqual(len(pr_test.co_owners.all()), 3)

    def test_personal_room_is_co_owner(self):
        gp = GeneralParameter.load()

        pr_test = PersonalRoom(
            name="testroom",
            owner=self.teacher_user_1,
            tenant=gp.home_room_tenant,
            is_public=False,
            config=self.test_room_config,
        )
        pr_test.save()

        pr_test.co_owners.add(self.teacher_user_1)
        pr_test.co_owners.add(self.teacher_user_2)
        pr_test.co_owners.add(self.student_user_1)
        pr_test.co_owners.add(self.student_user_2)
        pr_test.save()

        print("Owner: ", pr_test.owner)
        print("Co_Owner: ", pr_test.co_owners.all())

        self.assertEqual(pr_test.has_co_owner(self.teacher_user_1), False)
        self.assertEqual(pr_test.has_co_owner(self.teacher_user_2), True)
